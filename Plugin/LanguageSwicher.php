<?php
/**
 * This source file is part of content management system
 *
 * @category Structure
 * @package Structure_Plugin
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Changes the language of application
 * 
 * @category Structure
 * @package Structure_Plugin
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Structure_Plugin_LanguageSwicher
    extends Zend_Controller_Plugin_Abstract
{
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $lang = $request->getParam('lang', null);
        $translate = Zend_Registry::get('Zend_Translate');
         
        if ($translate->isAvailable($lang)) {
            $translate->setLocale($lang);
        } else {
            $translate->setLocale('en');
        }
        
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $router->setGlobalParam('lang', $lang);
    }
}
