<?php

class Structure_Plugin_ErrorSwicher
extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $moduleName = $request->getModuleName();
        $errorHandler = $this->_getErrorHandler();
        $testRequest
            = $this->_createTestRequestForModule($moduleName);
        if ($this->_isCorrectRequest($testRequest)) {
            $errorHandler->setErrorHandlerModule($moduleName);
        } else {
            $errorHandler->setErrorHandlerModule('default');
        }
    }
    
    private function _getFrontController()
    {
        $frontController = Zend_Controller_Front::getInstance();
        
        return $frontController;
    }
    
    private function _getErrorHandler()
    {
        $frontController = $this->_getFrontController();
        $errorHandler = $frontController->getPlugin(
                'Zend_Controller_Plugin_ErrorHandler'
        );
        
        return $errorHandler;
    }
    
    private function _createTestRequestForModule($moduleName)
    {
        $errorHandler = $this->_getErrorHandler();
        $testRequest = new Zend_Controller_Request_Http();
        $testRequest->setModuleName($moduleName);
        $testRequest->setControllerName($errorHandler->getErrorHandlerController());
        $testRequest->setActionName($errorHandler->getErrorHandlerAction());
        
        return $testRequest;
    }
    
    private function _isCorrectRequest(Zend_Controller_Request_Http $request)
    {
        $frontController = $this->_getFrontController();
        
        return $frontController->getDispatcher()
            ->isDispatchable($request);
    }
}