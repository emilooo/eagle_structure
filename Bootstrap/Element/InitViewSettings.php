<?php
/**
 * This source file is part of content management system
 *
 * @category Structure
 * @package Structure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the view settings through:
 * - sets the default encoding
 * - sets the settings of meta
 * - sets the title of page
 * 
 * @abstract
 * @category Structure
 * @package Structure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
abstract class Structure_Bootstrap_Element_InitViewSettings
extends Infrastructure_Bootstrap_Element_Abstract
{
    /**
     * Stores the instance of view
     * 
     * @var Zend_View
     */
    private $_view = null;
    
    public function initiate()
    {
        $this->_addEncoding();
        $this->_addHeadMeta();
        $this->_addHeadTitle();
    }
    
    /**
     * Returns the instance of view
     * 
     * @return Zend_View
     */
    protected function _getView()
    {
        if (empty($this->_view)) {
            $this->_view = new Zend_View();
        }
        
        return $this->_view;
    }
    
    /**
     * Adding the information about page encoding
     * 
     * @return string
     */
    abstract protected function _addEncoding();
    
    /**
     * Adding the information about page head meta
     * 
     * @return void
     */
    abstract protected function _addHeadMeta();
    
    /**
     * Adding the information about page head title
     */
    abstract protected function _addHeadTitle();
}
