<?php
/**
 * This source file is part of content management system
 *
 * @category Structure
 * @package Structure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the configuration of application logger functionality
 * 
 * @category Structure
 * @package Structure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Structure_Bootstrap_Element_InitZendLog
extends Infrastructure_Bootstrap_Element_Abstract
{
    public function initiate()
    {
        $this->getBootstrap()->bootstrap('frontController');
        $makeLogResult = $this->_makeLog();
        
        Zend_Registry::set('log', $makeLogResult);
    }
    
    /**
     * Build the logger
     * 
     * @return Zend_Log
     */
    private function _makeLog()
    {
        $zendLogger = new Zend_Log();
        $getWritterResult = $this->_makeWriter();
        $zendLogger->addWriter($getWritterResult);
        $getFilterResult = $this->_makeFilter();
        if (!empty($getFilterResult)) {
            $zendLogger->addFilter($getFilterResult);
        }
        
        return $zendLogger;
    }
    
    /**
     * Returns the stream writer
     * when environment of production is set else returns the writer of firebug
     * 
     * @return Zend_Log_Writer_Stream|Zend_Log_Writer_Firebug
     */
    private function _makeWriter()
    {
        $writer = 'production' == $this->getBootstrap()->getEnvironment() ?
            new Zend_Log_Writer_Stream(
                ROOT_PATH . '/data/logs/app.log'
            ) : new Zend_Log_Writer_Firebug();
        
        return $writer;
    }
    
    /**
     * If environment is production then
     * return the instance of filter else return null
     * 
     * @return Zend_Log_Filter_Priority|null
     */
    private function _makeFilter()
    {
        if ('production' == $this->getBootstrap()->getEnvironment()) {
            $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
            return $filter;
        }
    }
}