<?php
/**
 * This source file is part of content management system
 *
 * @category Structure
 * @package Structure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the smarty view compiler
 * 
 * @category Structure
 * @package Structure_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Structure_Bootstrap_Element_InitSmarty
extends Infrastructure_Bootstrap_Element_Abstract
{
    protected $_smartyOptions = array();
    
    public function initiate()
    {
        $view = $this->_prepareViewSmarty();
        $this->_prepareViewRenderer($view);
        $this->_prepareLayout();
        
        return $this;
    }
    
    /**
     * Prepares the instance of smarty to use
     * 
     * @return \Infrastructure_View_Smarty
     */
    private function _prepareViewSmarty()
    {
        $getSmartyConfigurationResult = $this->getBootstrap()
            ->getOption('smarty');
        $view = new Infrastructure_View_Smarty($getSmartyConfigurationResult);
        
        return $view;
    }
    
    /**
     * Prepares the settings of view renderer
     * 
     * @param Infrastructure_View_Smarty $view
     * @return Zend_Controller_Action_Helper_ViewRenderer
     */
    private function _prepareViewRenderer(Infrastructure_View_Smarty $view)
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
            'ViewRenderer'
        );
        $viewRenderer->setViewSuffix('tpl');
        $viewRenderer->setView($view);
        
        return $viewRenderer;
    }
    
    /**
     * Prepares the settings of view layout
     * 
     * @return Zend_Layout
     */
    private function _prepareLayout()
    {
        // ensure we have layout bootstraped
        $this->getBootstrap()->bootstrap('layout');
        // set the tpl suffix to layout also
        $layout = Zend_Layout::getMvcInstance();
        $layout->setViewSuffix('tpl');
        
        return $layout;
    }
}