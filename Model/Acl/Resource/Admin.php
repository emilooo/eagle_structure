<?php
/**
 * Provides the interface for admin resource
 * 
 * @package Structure
 * @package Structure_Model_Acl_Resource_Admin
 */
class Structure_Model_Acl_Resource_Admin
implements Zend_Acl_Resource_Interface
{
    public function getResourceId()
    {
        return 'Admin';
    }
}