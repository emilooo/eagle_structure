<?php
/**
 * Provides the interface for admin role
 * 
 * @package Structure
 * @package Structure_Model_Acl_Role_Admin
 */
class Structure_Model_Acl_Role_Admin
implements Zend_Acl_Role_Interface
{
    public function getRoleId()
    {
        return 'Admin';
    }
}