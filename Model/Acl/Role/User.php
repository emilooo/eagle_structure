<?php
/**
 * Provides the interface for user role
 * 
 * @package Structure
 * @package Structure_Model_Acl_Role_User
 */
class Structure_Model_Acl_Role_User
implements Zend_Acl_Role_Interface
{
    public function getRoleId()
    {
        return 'User';
    }
}