<?php
/**
 * Provides the interface for guest role
 * 
 * @package Structure
 * @package Structure_Model_Acl_Role_Guest
 */
class Structure_Model_Acl_Role_Guest
implements Zend_Acl_Role_Interface
{
    public function getRoleId()
    {
        return 'Guest';
    }
}