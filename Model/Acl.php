<?php
/**
 * Provides the interface for implements of access control list for models
 * 
 * @package Structure
 * @package Structure_Model_Acl
 */
class Structure_Model_Acl
extends Zend_Acl
implements Infrastructure_Acl_Interface
{
    public function __construct()
    {
        $this->addRole(new Structure_Model_Acl_Role_Guest);
        $this->addRole(new Structure_Model_Acl_Role_User, 'Guest');
        $this->addRole(new Structure_Model_Acl_Role_Admin, 'User');
        
        $this->deny();
        
        $this->add(new Structure_Model_Acl_Resource_Admin)->allow('Admin');
    }
}