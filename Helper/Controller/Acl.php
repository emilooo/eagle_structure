<?php
/**
 * @category Infrastructure
 * @package Infrastructure_Helper
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Infrastructure_Controller_Helper_Acl
 * 
 * Provides the interface for control access to resource.
 * We use when we do not have a Model
 * 
 * @category Infrastructure
 * @package Infrastructure_Helper
 * @subpackage Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Structure_Helper_Controller_Acl
extends Zend_Controller_Action_Helper_Abstract
{
    /**
     * @param Zend_Controller_Request_Http
     */
    protected $_request = null;
    
    /**
     * @var Zend_Auth
     */
    protected $_auth = null;
    
    /**
     * @var Zend_Acl
     */
    protected $_acl = null;
    
    /**
     * @var string
     */
    protected $_identity = null;
    
    protected $_login = null;
    
    public function __construct($options = array())
    {
        $this->_setOptions($options);
        $this->init();
    }
    
    /**
     * Sets default values to properties of class if properties are empty
     * 
     * @return void
     */
    public function init()
    {
        $this->_request
            = (empty($this->_request)) ? $this->getRequest() : $this->_request;
        $this->_auth
            = (empty($this->_auth)) ? Zend_Auth::getInstance() : $this->_auth;
    }
    
    /**
     * Returns the acl instance
     * 
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (empty($this->_acl)) {
            $this->_acl = $this->_getAclByModuleName();
        }
        
        return $this->_acl;
    }
    
    /**
     * Sets the user identity
     * 
     * @param string $identity
     * @return \Infrastructure_Controller_Helper_Acl
     */
    public function setIdentity($identity)
    {
        assert(
            $identity instanceof Zend_Db_Table_Row_Abstract,
            'User identity must be a instance of Zend_Db_Table_Row'
        );
        
        $this->_identity = $identity->__get('user_role');
        $this->_login = $identity->__get('user_login');
        
        return $this;
    }
    
    public function getLogin()
    {
        $getIdentityResult = $this->_login;
        if (empty($getIdentityResult)) {
            $getZendAuth = $this->_auth;
            if (!$getZendAuth->hasIdentity()) {
                return 'Guest';
            }
            $userIdentityResult = $getZendAuth->getIdentity();
            $this->setIdentity($userIdentityResult);
        }
        
        return $this->_login;
    }
    
    /**
     * Returns user identity
     * 
     * @return string
     */
    public function getIdentity()
    {
        $getIdentityResult = $this->_identity;
        if (empty($getIdentityResult)) {
            $getZendAuth = $this->_auth;
            if (!$getZendAuth->hasIdentity()) {
                return 'Guest';
            }
            $userIdentityResult = $getZendAuth->getIdentity();
            $this->setIdentity($userIdentityResult);
        }
        
        return $this->_identity;
    }
    
    /**
     * Cheks access to resource
     * 
     * @param string|null $resource Resource name
     * @param string|null $privilege
     * @return boolean
     */
    public function checkAcl($resource = null, $privilege = null)
    {
        assert((is_null($resource) || is_string($resource)));
        assert((is_null($privilege) || is_string($privilege)));
        
        $getAclResult = $this->getAcl();
        $getIsAllowedResult = $getAclResult->isAllowed(
            $this->getIdentity(),
            $resource,
            $privilege
        );
        
        return $getIsAllowedResult;
    }
    
    /**
     * Proxy to the checkAcl metod
     * 
     * @param string|null $resource
     * @param string|null $privilege
     * @return boolean
     */
    public function direct($resource = null, $privilege = null)
    {
        return $this->checkAcl($resource, $privilege);
    }
    
    /**
     * Returns the acl instance by module name
     * 
     * @return Zend_Acl
     */
    private function _getAclByModuleName()
    {
        assert($this->_request instanceof Zend_Controller_Request_Http);
        $getModuleNameResult = $this->_request->getModuleName();
        $getAclClassNameResult = ucfirst($getModuleNameResult)
            . '_Model_Acl_' . ucfirst($getModuleNameResult);
        
        if (class_exists($getAclClassNameResult)) {
            return new $getAclClassNameResult;
        }
        
        throw new Exception(
            'The class '.$getAclClassNameResult.' not exist!'
        );
    }
    
    /**
     * Saves the properties of class throught keys of array
     * 
     * Example:
     * array('key'=>'value') saves $this->_key => 'value'
     * 
     * @param array $options
     */
    private function _setOptions($options)
    {
        assert(is_array($options));
        
        if (!empty($options)) {
            foreach ($options as $optionKey => $optionValue) {
                $propertyName = '_' . $optionKey;
                if (property_exists($this, $propertyName)) {
                    $this->$propertyName = $optionValue;
                }
            }
        }
    }
}
